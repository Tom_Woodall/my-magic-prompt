passw_change(){
    fields=(${account[index]})
    echo "Write previous password"
    read -s passw_prev

    if [ "${passw_prev}" = "${fields[1]}" ]; then
        echo "New password"
        read -s passw_new

        account[$index]="${fields[0]} $passw_new ${fields[2]} ${fields[3]} ${fields[4]} ${fields[5]}"
        echo "${fields[1]}"
        echo "Change successful"
    else
        echo "Wrong password"
    fi
}


passw(){
    echo "do you want to change your password Y:n ?"
    read passw_responce

    case "${passw_responce}" in
        Y | y | Yes | yes)  passw_change;;
        N | n | No | no) echo "Ok, get out";;
        *) echo "Invalid input";;
    esac
}