#!/bin/bash
source quit.sh
source help.sh
source ls.sh
source rm.sh
source rmdir.sh
source about.sh
source version.sh
source age.sh
source pwd.sh
source hour.sh
source open.sh
source cd.sh
source httpget.sh
source auth.sh
source profil.sh
source passw.sh
source smtp.sh


cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | exit ) quit;;
    help ) help;;
    ls ) ls;;
    rm ) rm;;
    rmd | rmdir ) rmdir;;
    about ) about;;
    version | --v | vers ) version;;
    age ) age;;
    pwd ) pwd;;
    hour ) hour;;
    open ) open;;
    cd ) cd $2;;
    httpget ) httpget;;
    profil ) profil;;
    passw ) passw;;
    smtp ) smtp;;
    * ) echo "command not found";;
  esac
}

main() {
  lineCount=1

  auth

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mXzen\033[m ~ ☠️ ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main