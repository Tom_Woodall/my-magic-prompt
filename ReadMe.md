# My Magical Prompt!

This application lets you enter commands in a terminal.


# Installation

Run main.sh

## Launch

Login : access with your account
Inscription : create an account

## Command

**help**: List of the commands

**ls**: list file and directory

**rm** : delete a file

**rmd** or **rmdir** : delete a folder

**about** : description of the program

**version** | **--v** | **vers** : display the version of the prompt


**age** : ask your age and tells you whether you are an adult or a minor

**quit** : exit the prompt

**profile** : displays all your personal information

**passw** : allows you to change your password with a confirmation prompt *change are not saved upon exiting the app*

**cd** : go to a folder you've just created or return to a previous folder 
previous folder

**pwd** : indicates the current directory

**hour** : gives the current time

**httpget** : allows you to download the html source code of a web page and save it in a specific file. 

**smtp** : allows you to send an e-mail with an address, subject and body. *Might not work due to API issue*

**open**: open a file directly in the VIM editor, (even if the file doesn't exist *doesn't work*).




