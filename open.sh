open() {
    echo -ne "specify file name "
    read filename

    if [ -f $filename ]; then
        command vim $filename
        echo "file open"
    else 
        echo "file not found, try again "
    fi
}