rmdir() {
    echo -ne "specify directory name "
    read dirname

    if [ -f $dirname ]; then
        command rm -r $dirname
        echo "directory deleted"
    else 
        echo "directory not found, try again "
    fi
}